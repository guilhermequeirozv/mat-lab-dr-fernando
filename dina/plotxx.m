function [ax,hl1,hl2] = plotxx(x1,y1,x2,y2,limx1,limx2);
xlabels{1}='Tempo(s)'; 
xlabels{2}=' '; 
ylabels{1}='Força(kgf)'; 
ylabels{2}=' ';

hl1=line(x1,y1,'Color','k');
ax(1)=gca;
ax(1).XLim = [0 limx1];
set(ax(1),'Position',[0.12 0.12 0.75 0.70])
set(ax(1),'XColor','k','YColor','k');

ax(2)=axes('Position',get(ax(1),'Position'),...
   'XAxisLocation','top',...
   'YAxisLocation','right',...
   'Color','none',...
   'XColor','r','YColor','k');
ax(2).XLim = [0 limx2];

set(ax,'box','off')

hl2=line(x2,y2,'Color','r','Parent',ax(2));

%label the two x-axes
set(get(ax(1),'xlabel'),'string',xlabels{1})
set(get(ax(2),'xlabel'),'string',xlabels{2})
set(get(ax(1),'ylabel'),'string',ylabels{1})
set(get(ax(2),'ylabel'),'string',ylabels{2})
