prompt = {'Qual foi o tempo da coleta?(em segundos)'};
answers = inputdlg(prompt,'Dados do paciente',[1 35]);
if ~isempty(answers)
    [filePath,baseFile] = uigetfile('*');
    if ~isequal(filePath,0)
        currentFile = strcat(baseFile,filePath);
        currentFile = file_process(currentFile);
        dina_full = csvread(currentFile);
        sz = size(dina_full);
        s_y = linspace(0,str2double(answers(1)),sz(1));  
        s_y_2 = linspace(0,sz(1),sz(1));         
        plotxx(s_y,dina_full,s_y_2,dina_full,str2double(answers(1)),sz(1));
        message = sprintf('Pressione enter para come�ar.\nEscolha 2 pontos.\nPressione Enter Novamente.');
        xlabel(message);
        waitfor(gcf, 'CurrentCharacter', char(13));
        initial = getpts;
        x = fix(initial(1));
        y = fix(initial(2));
        Media_Dina = mean(dina_full(x:y));
        Max_Dina = max(dina_full(x:y));
        Min_Dina = min(dina_full(x:y));
        Amplitude_Dina = (Max_Dina-Min_Dina);
        figure(2);
        plot(s_y(x:y),dina_full(x:y),'g');
        file_path_sub = strrep(filePath,'_',' ');
        file_path_sub = strrep(file_path_sub,'.csv','');
        %mystr = sprintf('Tempo (s)\n%s',file_path_sub);
        mystr = sprintf('Tempo (s)');
        ylabel('For�a (kgf)');
        xlabel(mystr);
        saveas(gcf,file_path_sub,'jpeg');
        figure(3);
        barWidth = 0.25;
        b_y = [Media_Dina Max_Dina];
        b_x = categorical({'Media', 'Pico'});
        b = bar(b_x,b_y,'BarWidth',barWidth);
        %xtips = b(1).XEndPoints;
        %ytips = b(1).YEndPoints;
        ylim([0 Max_Dina+5]);
        labels = string(b(1).YData);
        xlabel(file_path_sub);
        %text(xtips,ytips,labels,'HorizontalAlignment','center','VerticalAlignment','bottom');
        text(1,Media_Dina,num2str(Media_Dina,'%g'),...
            'HorizontalAlignment','center','VerticalAlignment','bottom');
        text(2,Max_Dina,num2str(Max_Dina,'%g'),...
            'HorizontalAlignment','center','VerticalAlignment','bottom');
    else
        disp('Nenhum arquivo foi inserido. Tente novamente.');
    end
else
    disp('Insira as informacoes da coleta');
end


