function [new_file] = file_process(file)
fid = fopen(file,'r');
f = fread(fid,'*char')';
fclose(fid);
f = strrep(f,'" ","MicroFET2",','');
f = strrep(f,'" ","Device 1","kgf",','');
f = strrep(f,'" ","','');
f = strrep(f,'",','');
f = strrep(f,',','.');
f = strtrim(f);
new_file = strrep(file,".csv",'_fixed.csv');
fid = fopen(new_file,'w');
fprintf(fid,'%s',f);
fclose(fid);
end

